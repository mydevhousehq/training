# training

# software requirememnts
https://nodejs.org/en/ <br>
https://www.apachefriends.org/ <br>
https://code.visualstudio.com/ <br>
https://git-scm.com/downloads <br>
 
# library 
- gulp 
- bootstrap

 # Cloning
go to your xampp/docs create folder  <br>
open in terminal<br>
run code `git clone https://gitlab.com/mydevhousehq/training.git . ` <br>
run code `npm i gulp -g` <br>
run code `code .` <br>


# VScode settings 
- display terminal 
- use git bash 


```
 - edit -> gulpfile.js
 - look for


var paths = {
    styles: {
        src: 'style.scss', 
        dest: 'assets/css/'
    },
    scripts: {
        src: [
            'app.js'
        ],
        dest: 'assets/js/'
    },
    site: {
        url: 'http://localhost/training/'
    }
}


  usage: 
  css
  src: 'style.scss', - > location of your scss
  dest: 'assets/css/' - > destination of where scss file will be converted output -> asset/css/style.scss

  js
   'app.js' -> location of your js
   dest: 'assets/js/' destination of where js file will be converted output-> asset/js/app.js
```


# instructions

change gulpfile line 40 to your htdocs folder name<br>
before::
```
site: {
        url: 'http://localhost/training/'
    }
```
change to 
```
site: {
        url: 'http://localhost/YOURFOLDERNAMEWHEREYOUCLONED/'
    }
```

run in gitbash terminal vscode -> `npm i` 
<br>
run in gitbash terminal vscode -> `gulp` 
